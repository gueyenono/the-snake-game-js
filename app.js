document.addEventListener('DOMContentLoaded', () => {

  // DOM COMPONENTS

  // DIVs at startup
  const containerDiv = document.querySelector('.container') // container div
  const gameAndScoreDiv = document.querySelector('.game-and-score') // Game and score
  const gameAreaDiv = document.querySelector('.game-area')
  const scoreDiv = document.querySelector('.score')
  const scoreDisplay = document.querySelector('#score-display')

  // MAIN VARIABLES FOR THE GAME -----------------------------------------------

  let timerId // Timer ID
  let score = 0 // Score
  const gridSideLength = 50 // Number of cells in the grid is the square of this value


  // Create the game grid for the game (52 cells by 52 cells) (50 cells for snake movement and 2 cells are out-of-bounds)
  let gameAreaIndexes = [...Array((gridSideLength+2)*(gridSideLength+2)).keys()] // Add 2 cells for out-of-bounds
  gameAreaIndexes.forEach(index => {
    let div = document.createElement('div')
    gameAreaDiv.appendChild(div)
  })

  // Array of all divs in the snake grid
  const gameAreaCells = Array.from(document.querySelectorAll('.game-area div'))

  // Determine out-of-bounds cells and give them a class (out-of-bounds)
  let oob1 = [...Array(gridSideLength+2).keys()] // top edge
  let oob2 = createSeq(from = (gridSideLength+2)*((gridSideLength+2)-1)+1, to = (gridSideLength+2)*(gridSideLength+2)) // bottom edge 1
  oob2 = oob2.map(x => x - 1) // bottom edge 2
  let oob3 = createSeq(from = 1, to = gridSideLength+2).map(x => x*(gridSideLength+2) - 1) // right edge
  let oob4 = oob3.map(x => x + 1)  // left edge (1)
  oob4.splice(gridSideLength+2-1, 1) // left edge (2)
  oob4 = [0].concat(oob4) // left edge (3)

  const oobs = [...oob1, ...oob2, ...oob3, ...oob4]
  const outOfBoundsIndexes = [...new Set(oobs)] // Unique out of bounds indices
  const outOfBoundsCells = outOfBoundsIndexes.map(index => {
    gameAreaCells[index].classList.add('out-of-bounds')
    return gameAreaCells[index]
  })

  // Determine in-bounds cells and give them a class (in-bounds)
  const inBoundsIndexes = gameAreaIndexes.filter(cell => outOfBoundsIndexes.indexOf(cell) === -1)
  const inBoundsCells = inBoundsIndexes.map(index => {
    gameAreaCells[index].classList.add('in-bounds')
    return gameAreaCells[index]
  })

  // Snake variables
  let currentSnakeLength = Math.floor(gridSideLength*0.2) // Length of the snake when game starts
  const initialSnakeRow = 2 // Grid row where the snake is initialized
  let currentMovementDirection = 'right'
  let currentSnakePosition = createSeq(from = 0, to = currentSnakeLength-1).map(x => x + initialSnakeRow*(gridSideLength+2)+1) // Snake position is initialized
  let currentSnakeHeadPosition = currentSnakePosition[currentSnakeLength-1] // Position of the snake's head
  let currentSnakeTailPosition = 1 // Position of the snake's tail

  // Food variables
  let possiblePositionsForFoodIndexes = inBoundsIndexes.filter(cell => currentSnakePosition.indexOf(cell) === -1)
  let randomFoodPositionIndex


  // MAIN FUNCTIONS FOR THE GAME -----------------------------------------------

  // Create a function to make sequences
  function createSeq(from, to) {
    let seq = []
    for(let i = from; i < to+1; i++){
      seq.push(i)
    }
    return seq
  }

  // Function to get index of all occurrences of a value in an array
  function getAllIndexes(arr, val) {
      let indexes = [], i = -1;
      while ((i = arr.indexOf(val, i+1)) != -1){
          indexes.push(i);
      }
      return indexes;
  }

  // Draw snake on main grid (starts at initial position)
  function drawSnake() {
    currentSnakePosition.forEach(index => {
      gameAreaCells[index].classList.add('snake')
    })
  }

  // Remove snake from main grid
  function undrawSnake() {
    currentSnakePosition.forEach(index => {
      gameAreaCells[index].classList.remove('snake')
    })
  }

  // Draw food on the grid
  function drawFood() {
    randomFoodPositionIndex = possiblePositionsForFoodIndexes[Math.floor(Math.random()*possiblePositionsForFoodIndexes.length)]
    gameAreaCells[randomFoodPositionIndex].classList.add('food')
  }

  // Undraw food
  function unDrawFood() {
    gameAreaCells[randomFoodPositionIndex].classList.remove('food')
  }

  // Grow snake
  function growSnake() {
    if(diffLastTwoSnakeCells(currentSnakePosition) === -1) {
      currentSnakePosition = [currentSnakePosition[0]-1].concat(currentSnakePosition)
    } else if (diffLastTwoSnakeCells(currentSnakePosition) === 1) {
      currentSnakePosition = [currentSnakePosition[0]+1].concat(currentSnakePosition)
    } else if (diffLastTwoSnakeCells(currentSnakePosition) === -(gridSideLength+2)) {
      currentSnakePosition = [currentSnakePosition[0]-(gridSideLength+2)].concat(currentSnakePosition)
    } else if (diffLastTwoSnakeCells(currentSnakePosition) === (gridSideLength+2)) {
      currentSnakePosition = [currentSnakePosition[0]+(gridSideLength+2)].concat(currentSnakePosition)
    }
  }

  // When the snake bites itself
  function snakeBitItself() {
    let arr1 = getAllIndexes(currentSnakePosition, currentSnakeHeadPosition)
    return arr1.length === 2
  }

  // Update the snake head's position
  function updateSnakeHeadPosition() {
    if(currentMovementDirection === 'right') {
      currentSnakeHeadPosition += 1
    } else if(currentMovementDirection === 'left') {
      currentSnakeHeadPosition -= 1
    }  else if(currentMovementDirection === 'up') {
      currentSnakeHeadPosition -= (gridSideLength+2)
    } else if(currentMovementDirection === 'down') {
      currentSnakeHeadPosition += (gridSideLength+2)
    }
  }

  // Determines if the snake is out of bounds
  function isOutOfBounds() {
    return gameAreaCells[currentSnakeHeadPosition].classList.contains('out-of-bounds')
  }

  // Game over
  function gameOver() {
    currentMovementDirection = 'idle'
    clearInterval(timerId)
  }

  //  Assign functions to keycodes
  function control(e) {
    if(e.keyCode === 37 & currentMovementDirection != 'right') { // Press left
      currentMovementDirection = 'left'
    } else if(e.keyCode === 38 & currentMovementDirection != 'down') { // Press up
      currentMovementDirection = 'up'
    } else if(e.keyCode === 39 & currentMovementDirection != 'left') { // Press right
      currentMovementDirection = 'right'
    } else if(e.keyCode === 40 & currentMovementDirection != 'up') { // Press down
      currentMovementDirection = 'down'
    }

    moveSnake()
  }

  // Difference between indexes of last two snake cells
  function diffLastTwoSnakeCells(myArray) {
    let LastTwoSnakeCells = myArray.slice(0, 2)
    let diff = LastTwoSnakeCells[0] - LastTwoSnakeCells[1]
    return diff
  }

  // Move the Snake
  function moveSnake() {

    if(isOutOfBounds()){ // Snake is out of bounds
      alert('You hit the wall!')
      gameOver()
      return true
    }

    if(snakeBitItself()) { // snake bites itself
      alert('You bit yourself!')
      gameOver()
      return true
    }

    if(currentSnakeHeadPosition === randomFoodPositionIndex){ // Snake eats
      unDrawFood()
      growSnake()
      drawFood()
      score += 1
      scoreDisplay.innerHTML = score
      // console.log(score)
    }

    undrawSnake()
    updateSnakeHeadPosition()
    currentSnakePosition.splice(0, 1)
    currentSnakePosition = currentSnakePosition.concat(currentSnakeHeadPosition)
    drawSnake()
  }

  // GAME LOGIC -----------------------------------------------

  document.querySelector('#start-pause').addEventListener('click', () => {
    if(timerId){ // If the game is running, stop it
      clearInterval(timerId)
      timerId = null
    } else { // If the game is stopped, run it
      timerId = setInterval(moveSnake, 100)
    }
  })

  document.addEventListener('keyup', control)

  drawSnake()
  drawFood()

})
